'use strict';

module.exports = function setupAuth (api, controllers) {
  const { AuthController } = controllers;

  api.post('/logout', AuthController.logout);

  return api;
};
