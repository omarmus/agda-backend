'use strict';

module.exports = function setupCuentaRoute (api, controllers) {
  const { CuentaController } = controllers;

  api.post('/cuenta/archivo/upload', CuentaController.subirArchivo);

  return api;
};
