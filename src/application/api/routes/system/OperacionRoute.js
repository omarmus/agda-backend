'use strict';

module.exports = function setupOperacionRoute (api, controllers) {
  const { OperacionController } = controllers;

  api.post('/operacion/archivo/upload', OperacionController.subirArchivo);

  return api;
};
