'use strict';

module.exports = function setupDocumentoRoute (api, controllers) {
  const { DocumentoController } = controllers;

  api.post('/documento/cliente/upload', DocumentoController.subirDocumento);
  api.post('/documento/dui', DocumentoController.buscarDui);

  return api;
};
