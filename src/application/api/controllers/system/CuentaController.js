'use strict';

const debug = require('debug')('app:controller:token');
const { userData } = require('../../../lib/auth');

module.exports = function setupUsuarioController (services) {
  const { CuentaService } = services;

  async function subirArchivo (req, res, next) {
    debug('Subiendo archivo');

    if (!req.files) {
      return res.status(412).send({ error: 'Debe enviar el archivo.' });
    }

    let user = await userData(req, services);

    const { file } = req.files;
    try {
      let result = await CuentaService.subirArchivo(file, user.id);
      if (result.code === 1) {
        return res.send(result.data);
      } else {
        return res.status(412).send({ error: result.message });
      }
    } catch (e) {
      return next(e);
    }
  }

  return {
    subirArchivo
  };
};
