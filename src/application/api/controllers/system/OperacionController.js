'use strict';

const debug = require('debug')('app:controller:token');
const { userData } = require('../../../lib/auth');

module.exports = function setupUsuarioController (services) {
  const { OperacionService } = services;

  async function subirArchivo (req, res, next) {
    debug('Importando archivo MDR de operacion');

    if (!req.files) {
      return res.status(412).send({ error: 'Debe enviar el archivo.' });
    }

    const { idOperacion } = req.body;

    if (!idOperacion) {
      return res.status(412).send({ error: 'Debe enviar el ID del documento.' });
    }

    let user = await userData(req, services);

    const { pdf } = req.files;
    try {
      let result = await OperacionService.subirArchivo(pdf, idOperacion, user.id);
      if (result.code === 1) {
        return res.send(result.data);
      } else {
        return res.status(412).send({ error: result.message });
      }
    } catch (e) {
      return next(e);
    }
  }

  return {
    subirArchivo
  };
};
