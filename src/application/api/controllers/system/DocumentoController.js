'use strict';

const debug = require('debug')('app:controller:token');
const request = require('request');
const { userData } = require('../../../lib/auth');
const contentCss = require('../../../lib/content.css');
const { config } = require('../../../../common');

module.exports = function setupUsuarioController (services) {
  const { DocumentoService } = services;

  async function subirDocumento (req, res, next) {
    debug('Importando archivo MDR de operacion');

    if (!req.files) {
      return res.status(412).send({ error: 'Debe enviar el archivo Excel del MDR de la operación.' });
    }

    const { idDocumento, idCliente, tipo, idOperacion, update } = req.body;

    if (!idDocumento) {
      return res.status(412).send({ error: 'Debe enviar el ID del documento.' });
    }

    if (!idCliente) {
      return res.status(412).send({ error: 'Debe enviar el ID del cliente.' });
    }

    if (!tipo) {
      return res.status(412).send({ error: 'Debe enviar el tipo del documento.' });
    }

    const user = await userData(req, services);

    // console.log('req.files', req.files);
    const { pdf } = req.files;
    try {
      const result = await DocumentoService.subirDocumento(pdf, idDocumento, user.id, idCliente, tipo, idOperacion, update);
      if (result.code === 1) {
        res.send(result.data);
      } else {
        return res.status(412).send({ error: result.message });
      }
    } catch (e) {
      return next(e);
    }
  }

  async function buscarDui (req, res, next) {
    const { gestion, aduana, serie, numero } = req.body;
    request(config.db.aduanaLoginUrl, function (error, response) {
      if (error) {
        return res.status(412).send({ error: error.message || 'Ocurrió un error al tratar de conectarse con la Aduana' });
      }
      if (response.headers['set-cookie'] && response.headers['set-cookie'][0]) {
        const cookie = response.headers['set-cookie'][0].split(';')[0];
        request({
          method: 'POST',
          url: config.db.aduanaStatusUrl,
          form: {
            boton: 'Consultar',
            ops: 'S',
            gestion,
            aduana,
            serie,
            numero
          },
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Cookie: cookie
          }
        }, function (error, response, body) {
          if (error) {
            return res.status(412).send({ error: error.message || 'Ocurrió un error al consultar la DUI' });
          }

          if (response.statusCode === 200) {
            body = body.replace(/img\/registro.jpg/gi, `${config.db.backendUrl}/images/registro.jpg`);
            body = body.replace(/img\/moneda.jpg/gi, `${config.db.backendUrl}/images/moneda.jpg`);
            body = body.replace(/img\/verde.jpg/gi, `${config.db.backendUrl}/images/verde.jpg`);
            body = body.replace(/img\/ok1.jpg/gi, `${config.db.backendUrl}/images/ok1.jpg`);
            res.send({ html: contentCss + body });
          } else {
            return res.status(412).send({ error: 'No se pudo consultar la DUI' });
          }
        });
      } else {
        return res.status(412).send({ error: 'No se pudo obtener la cabecera' });
      }
    });
  }

  return {
    subirDocumento,
    buscarDui
  };
};
