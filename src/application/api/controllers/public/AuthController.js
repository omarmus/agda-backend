'use strict';

const debug = require('debug')('app:controller:auth');

module.exports = function setupAuthController (services) {
  const { UsuarioService } = services;

  async function login (req, res, next) {
    debug('Autenticación de usuario');

    const { contrasena, nit } = req.body;
    let { usuario } = req.body;
    let respuesta;

    try {
      if (!usuario || !contrasena) {
        return res.status(403).send({ error: 'El usuario y la contraseña son obligatorios' });
      }
      // Verificando que exista el usuario/contraseña
      let user = await UsuarioService.userExist(usuario, contrasena, nit);
      if (user.code === -1) {
        return res.status(403).send({ error: user.message });
      }
      user = user.data;
      respuesta = await UsuarioService.getResponse(user, req.ipInfo);
    } catch (e) {
      return next(e);
    }
    res.send(respuesta);
  }

  async function logout (req, res, next) {
    debug('Salir del sistema');

    try {
      res.send({ success: 'Se cerró correctamente' });
    } catch (e) {
      return next(e);
    }
  }

  return {
    login,
    logout
  };
};
