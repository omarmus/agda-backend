'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { ClienteService } = services;

  return {
    Query: {
      clientes: async (_, args, context) => {
        permissions(context, 'clientes:read|usuarios:read');

        let items = await ClienteService.findAll(args, context.rol, context.id_cliente);
        return response(items);
      },
      cliente: async (_, args, context) => {
        permissions(context, 'clientes:read');

        let item = await ClienteService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      clienteAdd: async (_, args, context) => {
        permissions(context, 'clientes:create');

        args.cliente._user_created = context.id_usuario;
        let item = await ClienteService.createOrUpdate(args.cliente);
        return response(item);
      },
      clienteEdit: async (_, args, context) => {
        permissions(context, 'clientes:update');

        args.cliente._user_updated = context.id_usuario;
        args.cliente._updated_at = new Date();
        args.cliente.id = args.id;
        let item = await ClienteService.createOrUpdate(args.cliente);
        return response(item);
      },
      clienteDelete: async (_, args, context) => {
        permissions(context, 'clientes:delete');

        let deleted = await ClienteService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
