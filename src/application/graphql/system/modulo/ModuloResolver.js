'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { ModuloService } = services;

  return {
    Query: {
      modulos: async (_, args, context) => {
        permissions(context, 'modulos:read');

        let items = await ModuloService.findAll(args);
        return response(items);
      },
      modulo: async (_, args, context) => {
        permissions(context, 'modulos:read');

        let item = await ModuloService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      moduloAdd: async (_, args, context) => {
        permissions(context, 'modulos:create');

        args.modulo._user_created = context.id_usuario;
        let item = await ModuloService.createOrUpdate(args.modulo);
        return response(item);
      },
      moduloEdit: async (_, args, context) => {
        permissions(context, 'modulos:update');

        args.modulo._user_updated = context.id_usuario;
        args.modulo._updated_at = new Date();
        args.modulo.id = args.id;
        let item = await ModuloService.createOrUpdate(args.modulo);
        return response(item);
      },
      moduloDelete: async (_, args, context) => {
        permissions(context, 'modulos:delete');

        let deleted = await ModuloService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
