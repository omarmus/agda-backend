'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { RolService } = services;

  return {
    Query: {
      roles: async (_, args, context) => {
        permissions(context, 'roles:read|usuarios:read');

        let items = await RolService.findAll(args, context.rol);
        return response(items);
      },
      rol: async (_, args, context) => {
        permissions(context, 'roles:read');

        let item = await RolService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      rolAdd: async (_, args, context) => {
        permissions(context, 'roles:create');

        args.rol._user_created = context.id_usuario;
        let item = await RolService.createOrUpdate(args.rol);
        return response(item);
      },
      rolEdit: async (_, args, context) => {
        permissions(context, 'roles:update');

        args.rol._user_updated = context.id_usuario;
        args.rol._updated_at = new Date();
        args.rol.id = args.id;
        let item = await RolService.createOrUpdate(args.rol);
        return response(item);
      },
      rolDelete: async (_, args, context) => {
        permissions(context, 'roles:delete');

        let deleted = await RolService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
