'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { PlantillaDocumentosService } = services;

  return {
    Query: {
      plantillaDocumentos: async (_, args, context) => {
        permissions(context, 'documentos:read');

        let items = await PlantillaDocumentosService.findAll(args, context.id_rol);
        return response(items);
      },
      plantillaDocumento: async (_, args, context) => {
        permissions(context, 'documentos:read');

        let item = await PlantillaDocumentosService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      plantillaDocumentoAdd: async (_, args, context) => {
        permissions(context, 'documentos:create');

        args.plantillaDocumento._user_created = context.id_usuario;
        let item = await PlantillaDocumentosService.createOrUpdate(args.plantillaDocumento);
        return response(item);
      },
      plantillaDocumentoEdit: async (_, args, context) => {
        permissions(context, 'documentos:update');

        args.plantillaDocumento._user_updated = context.id_usuario;
        args.plantillaDocumento._updated_at = new Date();
        args.plantillaDocumento.id = args.id;
        let item = await PlantillaDocumentosService.createOrUpdate(args.plantillaDocumento);
        return response(item);
      },
      plantillaDocumentoDelete: async (_, args, context) => {
        permissions(context, 'documentos:delete');

        let deleted = await PlantillaDocumentosService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
