'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { OperacionService } = services;

  return {
    Query: {
      operaciones: async (_, args, context) => {
        permissions(context, 'operaciones:read');

        let items = await OperacionService.findAll(args, context.id_rol);
        return response(items);
      },
      operacion: async (_, args, context) => {
        permissions(context, 'operaciones:read');

        let item = await OperacionService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      operacionAdd: async (_, args, context) => {
        permissions(context, 'operaciones:create');

        args.operacion._user_created = context.id_usuario;
        let item = await OperacionService.createOrUpdate(args.operacion);
        return response(item);
      },
      operacionEdit: async (_, args, context) => {
        permissions(context, 'operaciones:update');

        args.operacion._user_updated = context.id_usuario;
        args.operacion._updated_at = new Date();
        args.operacion.id = args.id;
        let item = await OperacionService.createOrUpdate(args.operacion);
        return response(item);
      },
      operacionDelete: async (_, args, context) => {
        permissions(context, 'operaciones:delete');

        let deleted = await OperacionService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
