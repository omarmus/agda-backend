'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { CuentaService } = services;

  return {
    Query: {
      cuentas: async (_, args, context) => {
        permissions(context, 'cuentas:read');

        let items = await CuentaService.findAll(args, context.id_rol);
        return response(items);
      },
      cuenta: async (_, args, context) => {
        permissions(context, 'cuentas:read');

        let item = await CuentaService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      cuentaAdd: async (_, args, context) => {
        permissions(context, 'cuentas:create');

        args.cuenta._user_created = context.id_usuario;
        let item = await CuentaService.createOrUpdate(args.cuenta);
        return response(item);
      },
      cuentaEdit: async (_, args, context) => {
        permissions(context, 'cuentas:update');

        args.cuenta._user_updated = context.id_usuario;
        args.cuenta._updated_at = new Date();
        args.cuenta.id = args.id;
        let item = await CuentaService.createOrUpdate(args.cuenta);
        return response(item);
      },
      cuentaDelete: async (_, args, context) => {
        permissions(context, 'cuentas:delete');

        let deleted = await CuentaService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
