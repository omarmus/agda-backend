'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { DocumentoService } = services;

  return {
    Query: {
      documentos: async (_, args, context) => {
        permissions(context, 'documentos:read|operaciones:read');

        let items = await DocumentoService.findAll(args, context.id_rol);
        return response(items);
      },
      documento: async (_, args, context) => {
        permissions(context, 'documentos:read|operaciones:read');

        let item = await DocumentoService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      documentoAdd: async (_, args, context) => {
        permissions(context, 'documentos:create');

        args.documento._user_created = context.id_usuario;
        let item = await DocumentoService.createOrUpdate(args.documento);
        return response(item);
      },
      documentoEdit: async (_, args, context) => {
        permissions(context, 'documentos:update');

        args.documento._user_updated = context.id_usuario;
        args.documento._updated_at = new Date();
        args.documento.id = args.id;
        let item = await DocumentoService.createOrUpdate(args.documento);
        return response(item);
      },
      documentoDelete: async (_, args, context) => {
        permissions(context, 'documentos:delete');

        let deleted = await DocumentoService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
