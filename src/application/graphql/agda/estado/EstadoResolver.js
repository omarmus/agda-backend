'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { EstadoService } = services;

  return {
    Query: {
      estados: async (_, args, context) => {
        permissions(context, 'estados:read');

        const items = await EstadoService.findAll(args, context.id_rol);
        return response(items);
      },
      estado: async (_, args, context) => {
        permissions(context, 'estados:read');

        const item = await EstadoService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      estadoAdd: async (_, args, context) => {
        permissions(context, 'estados:create');

        args.estado._user_created = context.id_usuario;
        const item = await EstadoService.createOrUpdate(args.estado);
        return response(item);
      },
      estadoEdit: async (_, args, context) => {
        permissions(context, 'estados:update');

        args.estado._user_updated = context.id_usuario;
        args.estado._updated_at = new Date();
        args.estado.id = args.id;
        const item = await EstadoService.createOrUpdate(args.estado, args.email);
        return response(item);
      },
      estadoDelete: async (_, args, context) => {
        permissions(context, 'estados:delete');

        const deleted = await EstadoService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
