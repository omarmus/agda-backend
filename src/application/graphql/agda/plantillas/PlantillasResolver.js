'use strict';
const { permissions, response } = require('../../../lib/auth');

module.exports = function setupResolver (services) {
  const { PlantillaService } = services;

  return {
    Query: {
      plantillas: async (_, args, context) => {
        permissions(context, 'documentos:read|operaciones:read');

        let items = await PlantillaService.findAll(args, context.id_rol);
        return response(items);
      },
      plantilla: async (_, args, context) => {
        permissions(context, 'documentos:read');

        let item = await PlantillaService.findById(args.id);
        return response(item);
      }
    },
    Mutation: {
      plantillaAdd: async (_, args, context) => {
        permissions(context, 'documentos:create');

        args.plantilla._user_created = context.id_usuario;
        let item = await PlantillaService.createOrUpdate(args.plantilla);
        return response(item);
      },
      plantillaEdit: async (_, args, context) => {
        permissions(context, 'documentos:update');

        args.plantilla._user_updated = context.id_usuario;
        args.plantilla._updated_at = new Date();
        args.plantilla.id = args.id;
        let item = await PlantillaService.createOrUpdate(args.plantilla);
        return response(item);
      },
      plantillaDelete: async (_, args, context) => {
        permissions(context, 'documentos:delete');

        let deleted = await PlantillaService.deleteItem(args.id);
        return { deleted: response(deleted) };
      }
    }
  };
};
