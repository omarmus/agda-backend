const css = `
<style>
html{font-size: 100%} 
body {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: .75em; margin: 0 1px auto 1px}
input, textarea, option {color: #3058A3;font-size: 10px;}
select {color: #3058A3;font-size: 9px;}
h2{color: #042A96;font-size: 1.5em;border-bottom: 1px solid #CAD6E8}
h5{color: #006699;font-size: 1em;border-bottom: 1px solid #CAD6E8}
#header{margin: 0 0 1em 0}
#header table{width: 100%}
#content{padding: 0 .5em}
#content .inner {
  width: 100%;
  margin: 0 .5em .75em;
}
#content2 {text-align:center}
#content2 .inner {}
#sidebar{padding: 0}
#footer{color: #7D7D75;padding: .5em;font-size: xx-small;}
#userdata{font-size: xx-small; color: #385B88;background-color: #EAFFEA; padding: .3em; margin: 0}
#menubar {
  float: left;
  width: 100%;
  height: 23px;
  padding:0;
  margin: 0 0 1em 0;
  font-family: Geneva, Arial, Helvetica, sans-serif;
  border-top: 1px solid #CAD6E8;
  border-bottom: 1px solid #CAD6E8;
  color: #385B88;
}
#search form {margin:0;padding:7px;border: 1px solid #E5E5E5; background-color: #F7EBE0}
#search select {margin: 2px 0 0;width:14em}
#search button {margin: 0;padding: 0;background-color: #fff;border: none}
#search .searchparam {padding: 5px;background-color: #fff;border: 1px solid #E5E5E5}
#search .searchparam input {border: none}

table.lista {border-collapse: collapse}
table.lista caption {
  color: #385B88;
  background-color: #F2F2F2;
  font-size: 1.25em;
  font-family: Georgia, "Times New Roman", Times, serif;
  font-variant: small-caps;
  text-align: left;
  padding: .3em .5em;
}
table.lista th { 
  color: #000000;
  background-color: #BFBFBF;
  font-weight: bold;
  padding: 3px 5px;
  border-right: 1px solid #fff;
  border-bottom: 1px solid #fff;
  text-align: right;
  font-size: 0.8em;
}
table.lista td {
  font-size: 0.8em;
  padding: 3px 5px;
}
.opcion {margin:0; padding: .3em}
.opcion a, .opcion a:link, .opcion a:visited {margin:.1em;padding: .3em;color: #BFB8BF;text-decoration: none} 
.opcion a:hover {margin:.1em;padding: .3em;font-weight: bolder;color: #ff6600}

table.formulario  {
  border-collapse: collapse;
  margin: 1em 0;
}
table.formulario caption{
  color: #FFF;
  background-color: #3058A3;
  padding: .5em;
  text-align: center;
  font-size: xx-small;
}
table.formulario tbody tr {
  border-bottom: 1px solid #EFEFEF;
}
table.formulario td {
  font-size: xx-small;
  padding: 3px 5px;
  border-bottom: 1px solid #BDBDBD;  
}
table.formulario th {
  font-weight: bold;
  padding: 3px 5px;
  text-align: left;
  font-size: 0.7em;
  height: 50px;
}
#sidebar .inner {
  margin: 0;
  padding: 0 .5em 0 0;
}
.navigation {
  background-color: white;
  margin: 0 0 1em;
}
.navigation h3 {
  background: url(../img/er.gif) no-repeat .5em center;
  border-bottom: 1px solid #E5E5E5;
  color: #99001A;
  margin: 0;
  padding: .3em 0 .3em 2em;
  font-size: x-small
}
.navigation ul {
  margin: 0;
  padding: 0;
  background: #FFFFFF;
  list-style-type: none
}
.navigation ul li {
  margin: 0;
  padding: 0;
  border-bottom: 1px solid #EEF5FD;
  font-family: Geneva, Arial, Helvetica, sans-serif
}
.navigation ul li a {
  background: url(../img/azul.png) no-repeat .5em center;
  display: block;
  margin: 0;
  padding: .3em 0 .3em 2.5em;
  text-decoration: none;
  border: none
}
.navigation ul li a:hover {
  background: #EEF5FD url(../img/carazul.png) no-repeat .5em center;
  border: none
}
.navigation ul li a:link, .navigation ul li a:visited {
  color: #006699;
}

.s50 {width: 100%; margin: 1em auto; }
.s40 {width: 40em; margin: 1em auto}
.s40a {width: 40em; margin-top: 1em;margin-right: 1em;  margin-bottom: 1em; margin-left: 10em;}
.s30 {width: 35em; margin: 3em auto}
.CentraLL { position:absolute; top:20%; left:35%; width:40%; height:60%; }
.form fieldset {margin: .3em 0 0; padding: 0 .5em .5em}
.form legend {padding: .5em}
.form label {font-weight: bold; float: left; width: 14em; text-align:right; padding: .5em}
.form br{clear: left}
.login {float: left; width: 30%; margin: 0;}
.param {float: left; width: 70%; }

tr.alt td {background: #ecf6fc}
tr.over td {color: #167116;background-color: #EAFFEA}
tr.over th {color: #FFE0CC;background-color: #99001A}

.page { vertical-align: middle;margin: .1em .5em;padding: .25em 1em;font-size: xx-small}
.page a.nextlink, .page a.nextlink:link, .page a.nextlink:visited {padding: 2px 4px;color: #0066CC}
.page a.nextlink:hover {border: #ff6600 1px solid; padding: 2px 4px; color: #ff6600;text-decoration: none}
.page .thispage {border: #cad6e8 1px solid; padding: 2px 4px}
.cinner {text-align: center;width:100%; margin: .5em 0;padding: 1em 0}
.orange, .orange:link, .orange:visited {margin:1em;padding: 1em 2em;border: #FFE0CC 1px solid;background-color: #FFE0CC;color: #ff6600;text-decoration: none} 
.orange:hover {margin:1em;padding: 1em 2em;font-weight: bolder;border: #ff6600 1px solid;background-color:#ff6600;color: #fff}

.dataform td {
  padding: .5em 1em;
  border: 1px solid #fff;
}
.dataform caption {
  color: #fff;
  background-color: #3e83c9;
  font-family: Georgia, "Times New Roman", Times, serif;
  text-align: left;
  padding: .3em .5em;
}
.empty{width: 100%}
.buttonlist {padding:0;margin:0;text-align: center}
.error {
  background: #FFFFCC url(../img/warning.png) no-repeat .5em center;
  vertical-align: middle;
  margin: 1em;
  padding: 1em 1em 1em 5.5em;
  border-width: 1px;
  border-style: solid;
  border-color: #CCCCCC #999999 #999999 #CCCCCC
  }
.error h2 {
  margin: .1em; 
  padding: .3em .5em;
  font-size: small;
  color: #ff0000;
  border-bottom: 1px solid #ff0000
}
.error li{
  font-size: x-small;
}
.info {
  background: #FFFFCC url(../img/warning.png) no-repeat 5px center;
  vertical-align: middle;
  margin: 1em;
  padding: 1.5em 1em 1.5em 5.5em;
  border-width: 1px;
  border-style: solid;
  border-color: #CCCCCC #999999 #999999 #CCCCCC
}
.xxx{
  color: #000000;
  background-color: #BFBFBF;
  border: 2px solid #ffffff;
}
.yyy{
  color: #ffffff;
  background-color: #7F7F7F;
  border: 2px solid #ffffff;
}
TD {
  font-size: 11px;
  font-family: Tahoma, Arial, Garamond;
}
.non{
  vertical-align: middle;
  background: #F0F8FF;
  color: #003366;
  height: 20px;
}
.par{
  vertical-align: middle;
  background-color: #E0EEFE;
  color: #003366;
  height: 20px;
}
.diffe{
  vertical-align: middle;
  color: #FF6200;
  height: 20px;
  font-weight: bold;  
}
.nocon{
  vertical-align: middle;
  color: #ff0000;
  height: 20px;
  font-weight: bold;  
}
.cocon{
  vertical-align: middle;
  color: #008F5B;
  height: 20px;
  font-weight: bold;  
}
.subtit{
  vertical-align: middle;
  background: #FFFFCC;
  color: #003366;
  height: 20px;
}
.boto {
  BACKGROUND: #F5F9FD; 
  BORDER-BOTTOM: #F5F9FD 1px solid; 
  BORDER-LEFT: #F5F9FD 1px solid; 
  BORDER-RIGHT: #F5F9FD 1px solid; 
  BORDER-TOP: #F5F9FD 1px solid;
  COLOR: #000066; 
  CURSOR: hand; 
  font-size: 10px; 
  font-family: Tahoma, Arial, Garamond;  width: 90px;
}
.beto {
  BACKGROUND: #E0EEFE; 
  BORDER-BOTTOM: #E0EEFE 1px solid; 
  BORDER-LEFT: #E0EEFE 1px solid; 
  BORDER-RIGHT: #E0EEFE 1px solid; 
  BORDER-TOP: #E0EEFE 1px solid;
  COLOR: #000066; 
  CURSOR: hand; 
  font-size: 10px; 
  font-family: Tahoma, Arial, Garamond;
  width: 90px;
}
#detalle {
  width: 100%;
  margin: 5%;
}
#detalle .der {
  float: left;
  width: 30%;
  margin: 0;
}
#detalle .izq {
  width: 70%;
  margin: 0;
}
.serv{
  vertical-align: middle;
  background-color: #CCFFFF;
  color: #003366;
  height: 20px;
}
.rubro{
  vertical-align: middle;
  background-color: #EAFFEA;
  color: #003366;
  height: 20px;
}
.total{
  vertical-align: middle;
  background-color: #E0EEFE;
  color: #003366;
  height: 20px;
}
.totalf{
  vertical-align: middle;
  background-color: #66FF66;
  color: #003366;
  height: 20px;
}
.data a:link, .data a:visited {
  color: #006699;}

form input { 
  display: none; 
}
  
  /* ==========================================================================
                                  L O G I N
   ========================================================================== */
.login, header {
  background: #1f3649;
  background: -moz-radial-gradient(center, ellipse cover, #1f3649 0%, #17253d 44%, #040d11 100%);
  background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, #1f3649), color-stop(44%, #17253d), color-stop(100%, #040d11));
  background: -webkit-radial-gradient(center, ellipse cover, #1f3649 0%, #17253d 44%, #040d11 100%);
  background: -o-radial-gradient(center, ellipse cover, #1f3649 0%, #17253d 44%, #040d11 100%);
  background: -ms-radial-gradient(center, ellipse cover, #1f3649 0%, #17253d 44%, #040d11 100%);
  background: radial-gradient(ellipse at center, #1f3649 0%, #17253d 44%, #040d11 100%);
}
.form-login {margin: 80 auto 30px; max-width: 360px;}
.form-login .input-group, .form-login .form-group {
  margin-bottom: 6px;
  margin-top: 4px;
}
.form-login .panel .panel-heading .panel-title {margin-bottom: 0; font-weight: 300;}
.login .btn-scroll-top {display: none;}
.login .footer {text-align: center; border-color: transparent;}
.login .footer2 {text-align: center; border-color: transparent;}
.login header {background: transparent;}
</style>
`;

module.exports = css;
