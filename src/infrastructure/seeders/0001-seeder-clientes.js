'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');

// Datos de producción
let items = [
  {
    nombre: 'AGDA S.A.T',
    descripcion: '',
    sigla: 'AGDA',
    email: 'contacto@agda.com.bo',
    telefonos: '',
    direccion: '',
    web: 'agda.com.bo',
    estado: 'ACTIVO',
    nit: ''
  }
];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  let personas = Array(4).fill().map((_, i) => {
    let item = {
      nombre: casual.company_name,
      descripcion: casual.text,
      sigla: casual.company_suffix,
      email: casual.email,
      telefonos: `${casual.phone},${casual.phone}`,
      direccion: casual.address,
      web: casual.url,
      estado: 'ACTIVO'
    };

    return item;
  });

  items = items.concat(personas);
}

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_clientes', items, {});
  },

  down (queryInterface, Sequelize) { }
};
