'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  {
    nombre: 'Documento general 1',
    fecha_solicitud: new Date(),
    fecha_actualizacion: new Date(),
    observacion: '',
    tipo: 'CLIENTE',
    estado: 'ACTIVO'
  },
  {
    nombre: 'Documento general 2',
    fecha_solicitud: new Date(),
    fecha_actualizacion: new Date(),
    observacion: '',
    tipo: 'CLIENTE',
    estado: 'ACTIVO'
  },
  {
    nombre: 'Documento general 3',
    fecha_solicitud: new Date(),
    fecha_actualizacion: new Date(),
    observacion: '',
    tipo: 'CLIENTE',
    estado: 'ACTIVO'
  },
  {
    nombre: 'Documento cliente 1',
    fecha_solicitud: new Date(),
    fecha_actualizacion: new Date(),
    observacion: '',
    tipo: 'OPERACION',
    estado: 'ACTIVO'
  },
  {
    nombre: 'Documento cliente 2',
    fecha_solicitud: new Date(),
    fecha_actualizacion: new Date(),
    observacion: '',
    tipo: 'OPERACION',
    estado: 'ACTIVO'
  },
  {
    nombre: 'Documento cliente 3',
    fecha_solicitud: new Date(),
    fecha_actualizacion: new Date(),
    observacion: '',
    tipo: 'OPERACION',
    estado: 'ACTIVO'
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('agda_documentos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
