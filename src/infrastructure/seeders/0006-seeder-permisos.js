'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 1
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 2
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 3
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 4
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 5
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 6
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 7
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 8
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 9
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 10
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 11
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 12
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 13
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 1,
    'id_modulo': 14
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 1
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 2
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 3
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 4
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 5
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 6
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 7
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 8
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 9
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 10
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 11
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 12
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 13
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 2,
    'id_modulo': 14
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 1
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 2
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 3
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 4
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 5
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 6
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 7
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 8
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 9
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 10
  },
  {
    'create': false,
    'read': false,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 11
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 12
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 13
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 3,
    'id_modulo': 14
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 1
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 2
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 3
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 4
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 5
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 6
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 7
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 8
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 9
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 10
  },
  {
    'create': false,
    'read': false,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 11
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 12
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 13
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 4,
    'id_modulo': 14
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 1
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 2
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 3
  },
  {
    'create': true,
    'read': true,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 4
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 5
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 6
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 7
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 8
  },
  {
    'create': false,
    'read': false,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 9
  },
  {
    'create': false,
    'read': true,
    'update': false,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 10
  },
  {
    'create': false,
    'read': false,
    'update': true,
    'delete': true,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 11
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 12
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 13
  },
  {
    'create': false,
    'read': true,
    'update': true,
    'delete': false,
    'csv': false,
    'id_rol': 5,
    'id_modulo': 14
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_permisos', items, {});
  },

  down (queryInterface, Sequelize) { }
};
