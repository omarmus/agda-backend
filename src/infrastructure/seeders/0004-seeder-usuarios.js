'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');
const { text } = require('../../common');
const contrasena = text.encrypt('123456');

// Datos de producción
let items = [
  {
    usuario: 'admin',
    contrasena,
    email: 'admin@agda.com.bo',
    estado: 'ACTIVO',
    cargo: 'Profesional',
    id_persona: 1,
    id_rol: 1,
    id_cliente: 1
  },
  {
    usuario: 'agda',
    contrasena,
    email: 'agda@agda.com.bo',
    estado: 'ACTIVO',
    cargo: 'Profesional',
    id_persona: 2,
    id_rol: 2,
    id_cliente: 1
  },
  {
    usuario: 'cliente',
    contrasena,
    email: 'cliente@agda.com.bo',
    estado: 'ACTIVO',
    cargo: 'Profesional',
    id_persona: 3,
    id_rol: 3,
    id_cliente: 1
  },
  {
    usuario: 'operador',
    contrasena,
    email: 'operador@agda.com.bo',
    estado: 'ACTIVO',
    cargo: 'Profesional',
    id_persona: 4,
    id_rol: 4,
    id_cliente: 1
  },
  {
    usuario: 'admincliente',
    contrasena,
    email: 'admincliente@agda.com.bo',
    estado: 'ACTIVO',
    cargo: 'Profesional',
    id_persona: 5,
    id_rol: 5,
    id_cliente: 1
  }
];

// Agregando datos aleatorios para desarrollo
if (typeof process.env.NODE_ENV === 'undefined' || process.env.NODE_ENV !== 'production') {
  let usuarios = Array(5).fill().map((_, i) => {
    let item = {
      usuario: casual.username,
      contrasena,
      email: casual.email,
      estado: casual.random_element(['ACTIVO', 'INACTIVO']),
      id_persona: casual.integer(6, 10),
      id_rol: casual.integer(2, 5),
      id_cliente: casual.integer(1, 5)
    };

    return item;
  });

  items = items.concat(usuarios);
}

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_usuarios', items, {});
  },

  down (queryInterface, Sequelize) { }
};
