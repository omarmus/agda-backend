'use strict';

const { setTimestampsSeeder } = require('../lib/util');

let items = [
  { nombre: 'SUPERADMIN', descripcion: 'Super Administrador', path: 'clientes' },
  { nombre: 'ADMIN', descripcion: 'Administrador', path: 'usuarios' },
  { nombre: 'CLIENTE', descripcion: 'Usuario', path: 'operaciones' },
  { nombre: 'OPERADOR', descripcion: 'Usuario', path: 'operaciones' },
  { nombre: 'CLIENTE_ADMIN', descripcion: 'Usuario', path: 'usuarios' }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('sys_roles', items, {});
  },

  down (queryInterface, Sequelize) { }
};
