'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t('fields.cliente_nombre')
    },
    codigo: {
      type: DataTypes.STRING(50)
    },
    descripcion: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t('fields.cliente_descripcion')
    },
    email: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.cliente_email')
    },
    sigla: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.cliente_sigla')
    },
    nit: {
      type: DataTypes.STRING(20),
      xlabel: lang.t('fields.cliente_nit')
    },
    telefonos: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.cliente_telefonos')
    },
    direccion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.cliente_direccion')
    },
    zona: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.cliente_zona')
    },
    ciudad: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.cliente_ciudad')
    },
    pais: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.cliente_pais')
    },
    web: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t('fields.cliente_web')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO', 'ELIMINADO'],
      defaultValue: 'ACTIVO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Clientes = sequelize.define('clientes', fields, {
    timestamps: false,
    tableName: 'sys_clientes'
  });

  return Clientes;
};
