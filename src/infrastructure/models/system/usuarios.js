'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    usuario: {
      type: DataTypes.STRING(100),
      unique: true,
      allowNull: false,
      xlabel: lang.t('fields.usuario')
    },
    contrasena: {
      type: DataTypes.STRING(255),
      xlabel: lang.t('fields.contrasena')
    },
    codigo_logra: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.codigo_logra')
    },
    email: {
      type: DataTypes.STRING(100),
      unique: true,
      xlabel: lang.t('fields.email')
    },
    email_verified_at: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.email_verified_at')
    },
    cargo: {
      type: DataTypes.STRING(100),
      xlabel: lang.t('fields.cargo')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO', 'PENDIENTE', 'BLOQUEADO'],
      defaultValue: 'ACTIVO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    movil: {
      type: DataTypes.STRING(50),
      xlabel: lang.t('fields.movil')
    },
    ultimo_login: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.ultimo_login')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Users = sequelize.define('usuarios', fields, {
    timestamps: false,
    tableName: 'sys_usuarios'
  });

  return Users;
};
