'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.nombre'),
    },
    valor: {
      type: DataTypes.TEXT,
      allowNull: false,
      xlabel: lang.t('fields.parametro_valor'),
    },
    titulo: {
      type: DataTypes.STRING(100),
      allowNull: false,
      xlabel: lang.t('fields.parametro_titulo'),
    },
    descripcion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.prametro_descripcion'),
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['ACTIVO', 'INACTIVO', 'ELIMINADO'],
      defaultValue: 'ACTIVO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Parametros = sequelize.define('parametros', fields, {
    timestamps: false,
    tableName: 'sys_parametros'
  });

  return Parametros;
};
