'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t('fields.nombre')
    },
    fecha_solicitud: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_solicitud')
    },
    fecha_actualizacion: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_actualizacion')
    },
    observacion: {
      type: DataTypes.TEXT,
      xlabel: lang.t('fields.observacion')
    },
    tipo: {
      type: DataTypes.ENUM,
      values: [
        'CLIENTE',
        'CLIENTE_DOCUMENTO',
        'OPERACION',
        'OPERACION_DOCUMENTO',
        'OPERACION_REGULARIZACION'
      ],
      defaultValue: 'CLIENTE',
      allowNull: false,
      xlabel: lang.t('fields.tipo')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['PENDIENTE', 'VERIFICADO', 'OBSERVADO', 'ACTIVO', 'INACTIVO'],
      defaultValue: 'PENDIENTE',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Documentos = sequelize.define('documentos', fields, {
    timestamps: false,
    tableName: 'agda_documentos'
  });

  return Documentos;
};
