'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nro_factura: {
      type: DataTypes.STRING(100)
    },
    nro_referencia: {
      type: DataTypes.STRING(100)
    },
    nro_orden: {
      type: DataTypes.STRING(100)
    },
    tipo_importacion: {
      type: DataTypes.ENUM,
      values: ['CONSUMO_DIRECTO', 'ANTICIPADA', 'INMEDIATA', 'ABREVIADA'],
      defaultValue: 'CONSUMO_DIRECTO'
    },
    monto_pago: {
      type: DataTypes.DECIMAL,
      defaultValue: 0
    },
    opcion_pago: {
      type: DataTypes.STRING(50)
    },
    fecha_pago: {
      type: DataTypes.DATE
    },
    dui_gestion: {
      type: DataTypes.INTEGER(4)
    },
    dui_aduana: {
      type: DataTypes.INTEGER(3)
    },
    dui_serie: {
      type: DataTypes.STRING(1)
    },
    dui_numero: {
      type: DataTypes.INTEGER(11)
    },
    tipo: {
      type: DataTypes.ENUM,
      values: ['IMPORTACION', 'EXPORTACION'],
      defaultValue: 'IMPORTACION',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    },
    estado: {
      type: DataTypes.ENUM,
      values: ['PROCESO', 'PENDIENTE_PAGO', 'PENDIENTE_REGULARIZACION', 'SUJETO_MULTA', 'CONCLUIDO'],
      defaultValue: 'PROCESO',
      allowNull: false,
      xlabel: lang.t('fields.estado')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  const Operaciones = sequelize.define('operaciones', fields, {
    timestamps: false,
    tableName: 'agda_operaciones'
  });

  return Operaciones;
};
