'use strict';

const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let PlantillaDocumentos = sequelize.define('plantilla_documentos', fields, {
    timestamps: false,
    tableName: 'agda_plantilla_documentos'
  });

  return PlantillaDocumentos;
};
