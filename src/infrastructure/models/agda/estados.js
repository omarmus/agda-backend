'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
      xlabel: lang.t('fields.nombre')
    },
    observacion: {
      type: DataTypes.TEXT
    },
    fecha_solicitud: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_solicitud')
    },
    fecha_inicio: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_inicio')
    },
    fecha_final: {
      type: DataTypes.DATE,
      xlabel: lang.t('fields.fecha_final')
    },
    descripcion: {
      type: DataTypes.TEXT
    },
    orden: {
      type: DataTypes.INTEGER
    },
    estado: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      xlabel: lang.t('fields.estado')
    }
  };

  // Agregando campos para el log
  fields = util.setTimestamps(fields);

  let Estados = sequelize.define('estados', fields, {
    timestamps: false,
    tableName: 'agda_estados'
  });

  return Estados;
};
