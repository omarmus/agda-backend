'use strict';

// Definiendo asociaciones de las tablas
module.exports = function associations (models) {
  const {
    roles,
    usuarios,
    clientes,
    modulos,
    permisos,
    personas,
    archivos,
    documentos,
    operaciones,
    estados,
    cuentas,
    plantillas,
    plantilla_documentos: plantillaDocumentos
  } = models;

  // MODULO USUARIOS

  // Asociaciones table clientes
  clientes.belongsTo(personas, { foreignKey: { name: 'id_responsable' }, as: 'responsable' });
  personas.hasMany(clientes, { foreignKey: { name: 'id_responsable' }, as: 'responsable' });

  // Asociaciones tabla usuarios
  usuarios.belongsTo(clientes, { foreignKey: { name: 'id_cliente', allowNull: false }, as: 'cliente' });
  clientes.hasMany(usuarios, { foreignKey: { name: 'id_cliente', allowNull: false }, as: 'cliente' });

  usuarios.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  roles.hasMany(usuarios, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });

  usuarios.belongsTo(personas, { foreignKey: { name: 'id_persona' }, as: 'persona' });
  personas.hasMany(usuarios, { foreignKey: { name: 'id_persona' }, as: 'persona' });

  // Asociaciones tablas permisos - roles
  permisos.belongsTo(roles, { foreignKey: { name: 'id_rol', allowNull: false }, as: 'rol' });
  roles.hasMany(permisos, { foreignKey: { name: 'id_rol', allowNull: false } });

  // Asociaciones tablas permisos - modulos
  permisos.belongsTo(modulos, { foreignKey: { name: 'id_modulo', allowNull: false }, as: 'modulo' });
  modulos.hasMany(permisos, { foreignKey: { name: 'id_modulo', allowNull: false } });

  // Asociaciones tablas modulos - sección
  modulos.belongsTo(modulos, { foreignKey: 'id_modulo' });
  modulos.hasMany(modulos, { foreignKey: 'id_modulo' });

  modulos.belongsTo(modulos, { foreignKey: 'id_seccion' });
  modulos.hasMany(modulos, { foreignKey: 'id_seccion' });

  // AGDA

  // Asociaciones tablas documentos
  documentos.belongsTo(clientes, { foreignKey: { name: 'id_cliente' }, as: 'cliente' });
  clientes.hasMany(documentos, { foreignKey: { name: 'id_cliente' } });

  documentos.belongsTo(archivos, { foreignKey: { name: 'id_archivo' }, as: 'archivo' });
  archivos.hasMany(documentos, { foreignKey: { name: 'id_archivo' } });

  documentos.belongsTo(operaciones, { foreignKey: { name: 'id_operacion' }, as: 'operacion' });
  operaciones.hasMany(documentos, { foreignKey: { name: 'id_operacion' } });

  documentos.belongsTo(documentos, { foreignKey: 'id_documento_padre' });
  documentos.hasMany(documentos, { foreignKey: 'id_documento_padre' });

  // Asociaciones tablas operaciones
  operaciones.belongsTo(clientes, { foreignKey: { name: 'id_cliente' }, as: 'cliente' });
  clientes.hasMany(operaciones, { foreignKey: { name: 'id_cliente' } });

  operaciones.belongsTo(archivos, { foreignKey: { name: 'id_archivo_pago' }, as: 'archivo_pago' });
  archivos.hasMany(operaciones, { foreignKey: { name: 'id_archivo_pago' } });

  // Asociaciones tablas estados
  estados.belongsTo(operaciones, { foreignKey: { name: 'id_operacion' }, as: 'operacion' });
  operaciones.hasMany(estados, { foreignKey: { name: 'id_operacion' } });

  // Asociaciones tablas cuentas
  cuentas.belongsTo(clientes, { foreignKey: { name: 'id_cliente' }, as: 'cliente' });
  clientes.hasMany(cuentas, { foreignKey: { name: 'id_cliente' } });

  cuentas.belongsTo(archivos, { foreignKey: { name: 'id_archivo' }, as: 'archivo' });
  archivos.hasMany(cuentas, { foreignKey: { name: 'id_archivo' } });

  // // Asociaciones tablas plantilla_documentos
  plantillaDocumentos.belongsTo(plantillas, { foreignKey: { name: 'id_plantilla' }, as: 'plantilla' });
  plantillas.hasMany(plantillaDocumentos, { foreignKey: { name: 'id_plantilla' } });

  plantillaDocumentos.belongsTo(documentos, { foreignKey: { name: 'id_documento' }, as: 'documento' });
  documentos.hasMany(plantillaDocumentos, { foreignKey: { name: 'id_documento' } });

  return models;
};
