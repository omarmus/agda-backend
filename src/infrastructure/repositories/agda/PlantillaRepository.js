'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function plantillasRepository (models, Sequelize) {
  const { plantillas } = models;
  // const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [

    ];

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return plantillas.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, plantillas),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, plantillas, t),
    deleteItem: (id, t) => Repository.deleteItem(id, plantillas, t)
  };
};
