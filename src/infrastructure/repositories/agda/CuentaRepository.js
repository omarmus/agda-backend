'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function cuentasRepository (models, Sequelize) {
  const { cuentas, clientes, archivos } = models;
  // const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: [
          'nombre',
          'sigla'
        ],
        model: clientes,
        as: 'cliente'
      },
      {
        attributes: [
          'filename',
          'path',
          '_created_at'
        ],
        model: archivos,
        as: 'archivo'
      }
    ];

    if (params.id_cliente) {
      query.where.id_cliente = params.id_cliente;
    }

    if (params.id_archivo) {
      query.where.id_archivo = params.id_archivo;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return cuentas.findAndCountAll(query);
  }

  async function findById (id) {
    const result = await cuentas.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: [
            'nombre',
            'sigla'
          ],
          model: clientes,
          as: 'cliente'
        },
        {
          attributes: [
            'filename',
            'path',
            '_created_at'
          ],
          model: archivos,
          as: 'archivo'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, cuentas, t),
    deleteItem: (id, t) => Repository.deleteItem(id, cuentas, t)
  };
};
