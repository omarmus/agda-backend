'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function estadosRepository (models, Sequelize) {
  const { estados, operaciones, clientes } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    const query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: [
          'nro_factura',
          'nro_referencia',
          'nro_orden'
        ],
        model: operaciones,
        as: 'operacion',
        duplicating: true
      }
    ];

    if (params.id_operacion) {
      query.where.id_operacion = params.id_operacion;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return estados.findAndCountAll(query);
  }

  async function findOperations () {
    const result = await estados.findAndCountAll({
      where: {
        nombre: 'FECHA DE ARRIBO',
        estado: true,
        fecha_final: {
          [Op.not]: null
        },
        '$operacion.estado$': 'PENDIENTE_REGULARIZACION',
        '$operacion.tipo_importacion$': {
          [Op.in]: ['ANTICIPADA', 'INMEDIATA']
        }
      },
      include: [
        {
          attributes: [
            'nro_factura',
            'nro_referencia',
            'nro_orden',
            'estado',
            'tipo_importacion'
          ],
          model: operaciones,
          as: 'operacion',
          include: [
            {
              attributes: [
                'nombre',
                'email'
              ],
              model: clientes,
              as: 'cliente'
            }
          ]
        }
      ]
    });

    return toJSON(result);
  }

  return {
    findAll,
    findOperations,
    findById: (id) => Repository.findById(id, estados),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, estados, t),
    deleteItem: (id, t) => Repository.deleteItem(id, estados, t)
  };
};
