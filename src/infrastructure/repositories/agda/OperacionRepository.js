'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function operacionesRepository (models, Sequelize) {
  const { operaciones, clientes, archivos } = models;
  // const Op = Sequelize.Op;

  function findAll (params = {}) {
    const query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: [
          'nombre',
          'nit',
          'estado',
          'codigo',
          'email'
        ],
        model: clientes,
        as: 'cliente'
      },
      {
        attributes: [
          'filename',
          'path'
        ],
        model: archivos,
        as: 'archivo_pago'
      }
    ];

    if (params.id_cliente) {
      query.where.id_cliente = params.id_cliente;
    }

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return operaciones.findAndCountAll(query);
  }

  async function findById (id) {
    const result = await operaciones.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: [
            'nombre',
            'nit',
            'estado',
            'codigo',
            'email'
          ],
          model: clientes,
          as: 'cliente'
        },
        {
          attributes: [
            'filename',
            'path'
          ],
          model: archivos,
          as: 'archivo_pago'
        }
      ]
    });
    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, operaciones, t),
    deleteItem: (id, t) => Repository.deleteItem(id, operaciones, t)
  };
};
