'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function documentosRepository (models, Sequelize) {
  const { documentos, archivos } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    const query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: [
          'path',
          'filename',
          'nombre'
        ],
        model: archivos,
        as: 'archivo'
      }
    ];

    if (params.only) {
      query.where.tipo = {
        [Op.in]: ['CLIENTE', 'OPERACION']
      };
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    if (params.id_archivo) {
      query.where.id = params.id_archivo;
    }

    if (params.id_cliente) {
      query.where.id_cliente = params.id_cliente;
    }

    if (params.id_operacion) {
      query.where.id_operacion = params.id_operacion;
    }

    if (params.tipo) {
      query.where.tipo = params.tipo;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return documentos.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, documentos),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, documentos, t),
    deleteItem: (id, t) => Repository.deleteItem(id, documentos, t)
  };
};
