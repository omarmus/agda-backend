'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function plantillaDocumentosRepository (models, Sequelize) {
  const { plantilla_documentos: plantillaDocumentos, documentos } = models;
  // const Op = Sequelize.Op;

  async function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        model: documentos,
        as: 'documento'
      }
    ];

    if (params.id_plantilla) {
      query.where.id_plantilla = params.id_plantilla;
    }

    if (params.id_documento) {
      query.where.id_documento = params.id_documento;
    }

    let result = await plantillaDocumentos.findAndCountAll(query);
    return toJSON(result);
  }

  async function findById (id) {
    const result = await plantillaDocumentos.findByPk(id, {
      include: [
        {
          model: documentos,
          as: 'documento'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, plantillaDocumentos, t),
    deleteItem: (id, t) => Repository.deleteItem(id, plantillaDocumentos, t)
  };
};
