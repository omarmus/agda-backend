'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function archivosRepository (models, Sequelize) {
  const { archivos } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    if (params.filename) {
      query.where.filename = {
        [Op.iLike]: `%${params.filename}%`
      };
    }

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    if (params.mimetype) {
      query.where.mimetype = params.mimetype;
    }

    if (params.path) {
      query.where.path = params.path;
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    return archivos.findAndCountAll(query);
  }

  return {
    findAll,
    findById: (id) => Repository.findById(id, archivos),
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, archivos, t),
    deleteItem: (id, t) => Repository.deleteItem(id, archivos, t)
  };
};
