'use strict';

const { getQuery } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function clientesRepository (models, Sequelize) {
  const { clientes, personas } = models;
  const Op = Sequelize.Op;

  function findAll (params = {}) {
    let query = getQuery(params);
    query.where = {};

    query.include = [
      {
        attributes: [
          'nombre_completo',
          'tipo_documento',
          'nro_documento'
        ],
        model: personas,
        as: 'responsable'
      }
    ];

    if (params.nombre) {
      query.where.nombre = {
        [Op.iLike]: `%${params.nombre}%`
      };
    }

    if (params.sigla) {
      query.where.sigla = {
        [Op.iLike]: `%${params.sigla}%`
      };
    }

    if (params.estado) {
      query.where.estado = params.estado;
    }

    if (params.id_cliente) {
      query.where.id = params.id_cliente;
    }

    return clientes.findAndCountAll(query);
  }

  async function findById (id) {
    const result = await clientes.findOne({
      where: {
        id
      },
      include: [
        {
          attributes: [
            'nombre_completo',
            'tipo_documento',
            'nro_documento'
          ],
          model: personas,
          as: 'responsable'
        }
      ]
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  async function findByNit (nit) {
    const result = await clientes.findOne({
      where: {
        nit
      }
    });
    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findByNit,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, clientes, t),
    deleteItem: (id, t) => Repository.deleteItem(id, clientes, t)
  };
};
