'use strict';

const debug = require('debug')('app:service:cliente');
const Service = require('../Service');

module.exports = function clienteService (repositories, valueObjects, res) {
  const { ClienteRepository, PersonaRepository } = repositories;
  const {
    ClienteNombre,
    ClienteDescripcion,
    ClienteSigla,
    ClienteEmail,
    ClienteTelefonos,
    ClienteDireccion,
    ClienteWeb,
    ClienteEstado,
    ClienteSubdomain,
    ClienteCodigoPortal,
    ClienteNit,
    ClienteUsuario
  } = valueObjects;

  async function findAll (params = {}, rol, idCliente) {
    debug('Lista de cliente|filtros');

    switch (rol) {
      case 'CLIENTE': case 'CLIENTE_ADMIN':
        params.id_cliente = idCliente;
        break;
    }

    return Service.findAll(params, ClienteRepository, res, 'Cliente');
  }

  async function findById (id) {
    debug('Buscando cliente por ID', id);

    return Service.findById(id, ClienteRepository, res, 'Cliente');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar cliente');

    validate(data);

    let cliente;
    let persona;
    try {
      if (data.nro_documento || data.nombre_completo) {
        persona = {
          id: data.id_responsable,
          nro_documento: data.nro_documento,
          nombre_completo: data.nombre_completo
        };

        if (data.id_responsable) { // Actualizando persona
          persona._user_updated = data._user_updated;
          persona._updated_at = data._updated_at;
        } else {
          persona.estado = 'ACTIVO';
          persona._user_created = data._user_created || data._user_updated;
        }

        persona = await PersonaRepository.createOrUpdate(persona);
        data.id_responsable = persona.id;
      }

      cliente = await ClienteRepository.createOrUpdate(data);
    } catch (e) {
      return res.error(e);
    }

    if (!cliente) {
      return res.warning(new Error(`El cliente no pudo ser creado`));
    }

    return res.success(cliente);
  }

  async function deleteItem (id) {
    debug('Eliminando cliente');

    return Service.deleteItem(id, ClienteRepository, res, 'Cliente');
  }

  function validate (data) {
    Service.validate(data, {
      nombre: ClienteNombre,
      descripcion: ClienteDescripcion,
      sigla: ClienteSigla,
      email: ClienteEmail,
      telefonos: ClienteTelefonos,
      direccion: ClienteDireccion,
      web: ClienteWeb,
      estado: ClienteEstado,
      subdomain: ClienteSubdomain,
      codigo_portal: ClienteCodigoPortal,
      nit: ClienteNit,
      usuario: ClienteUsuario
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
