'use strict';

const debug = require('debug')('app:service:plantilla');
const Service = require('../Service');

module.exports = function plantillaService (repositories, valueObjects, res) {
  const { PlantillaDocumentosRepository } = repositories;
  // const {
  //   PlantillaDocumentosNombre,
  //   PlantillaDocumentosFechaSolicitud,
  //   PlantillaDocumentosFechaActualizacion,
  //   PlantillaDocumentosObservacion,
  //   PlantillaDocumentosEstado
  // } = valueObjects;

  async function findAll (params = {}, rol) {
    debug('Lista de plantilla|filtros');

    let lista;

    try {
      lista = await PlantillaDocumentosRepository.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.warning(new Error(`Error al obtener la lista de PlantillaDocumentos`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando plantilla por ID', id);

    return Service.findById(id, PlantillaDocumentosRepository, res, 'PlantillaDocumentos');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar plantilla');

    // validate(data);

    return Service.createOrUpdate(data, PlantillaDocumentosRepository, res, 'PlantillaDocumentos');
  }

  async function deleteItem (id) {
    debug('Eliminando plantilla');

    return Service.deleteItem(id, PlantillaDocumentosRepository, res, 'PlantillaDocumentos');
  }

  // function validate (data) {
  //   Service.validate(data, {
  //     nombre: PlantillaDocumentosNombre,
  //     fecha_solicitud: PlantillaDocumentosFechaSolicitud,
  //     fecha_actualizacion: PlantillaDocumentosFechaActualizacion,
  //     observacion: PlantillaDocumentosObservacion,
  //     estado: PlantillaDocumentosEstado
  //   });
  // }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
