'use strict';

const debug = require('debug')('app:service:documento');
const Service = require('../Service');
const path = require('path');
const fs = require('fs');
const moment = require('moment');

module.exports = function documentoService (repositories, valueObjects, res) {
  const { OperacionRepository, EstadoRepository, ArchivoRepository } = repositories;
  const {
    OperacionNroFactura,
    OperacionNroReferencia,
    OperacionTipo,
    OperacionTipoImportacion,
    OperacionEstado
  } = valueObjects;

  async function subirArchivo (file, idOperacion, idUsuario) {
    try {
      const filename = `DOC-${idOperacion}-${Date.now()}.${file.name.split('.').pop()}`;
      const filepath = `public/uploads/${filename}`;
      const filesize = (file.size / 1024).toFixed(2);
      console.log('ARCHIVO', file);

      // Subiendo archivo
      await file.mv(path.join(process.cwd(), filepath));
      if (!fs.existsSync(path.join(process.cwd(), filepath))) {
        throw new Error('Se produjo un error al subir el archivo.');
      }

      // crear archivo en la base de datos
      const archivo = await ArchivoRepository.createOrUpdate({
        filename: filename,
        path: filepath,
        es_imagen: false,
        size: filesize,
        _user_created: idUsuario,
        _created_at: new Date()
      });

      if (!archivo) {
        return res.error(new Error('Ocurrió un error al subir la información de archivo!'));
      }

      await OperacionRepository.createOrUpdate({
        id: idOperacion,
        id_archivo_pago: archivo.id
      });

      return res.success({ archivo });
    } catch (e) {
      return res.error(e);
    }
  }

  async function findAll (params, rol) {
    debug('Lista de documento|filtros');

    let lista;

    try {
      lista = await OperacionRepository.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.warning(new Error('Error al obtener la lista de Operacion'));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando documento por ID', id);

    return Service.findById(id, OperacionRepository, res, 'Operacion');
  }

  async function createOrUpdate (data) {
    let item;
    try {
      validate(data);

      if (data.fecha_finalizacion) {
        data.fecha_finalizacion = moment(data.fecha_finalizacion, 'YYYY-MM-DD HH:mm').toDate();
      }

      item = await OperacionRepository.createOrUpdate(data);
      if (!data.id) {
        const estado = {
          fecha_solicitud: new Date(),
          _user_created: data._user_created,
          _created_at: new Date(),
          id_operacion: item.id
        };
        const estados = [
          { nombre: 'OTROS', descripcion: 'Gestión de documentos requeridos' },
          { nombre: 'DAV', descripcion: 'Generación de la Declaración Andina de Valor' },
          { nombre: 'SICOIN', descripcion: 'Cargado de datos Aduana Nacional de Bolivia' },
          { nombre: 'VALIDACIÓN', descripcion: 'Generación de la Declaración Única de Importación (DUI)' },
          { nombre: 'DIGITALIZACIÓN', descripcion: 'Digitalización de documentos para el sistema de la Aduana Nacional de Bolivia' },
          { nombre: 'PAGO DE TRIBUTOS', descripcion: 'Pago de Tributos' },
          { nombre: 'FECHA DE ARRIBO', descripcion: 'Fecha de arribo del último camión' },
          { nombre: 'DESPACHO CONCLUÍDO', descripcion: 'Fin del proceso de despacho' }
        ];
        for (let i = 0, l = estados.length; i < l; i++) {
          estado.nombre = estados[i].nombre;
          estado.descripcion = estados[i].descripcion;
          estado.orden = i + 1;
          await EstadoRepository.createOrUpdate(estado);
        }
      }
    } catch (e) {
      return res.error(e);
    }

    if (!item) {
      return res.warning(new Error('El Operación no pudo ser creado'));
    }

    return res.success(item);
  }

  async function deleteItem (id) {
    debug('Eliminando documento');

    return Service.deleteItem(id, OperacionRepository, res, 'Operacion');
  }

  function validate (data) {
    Service.validate(data, {
      nro_factura: OperacionNroFactura,
      nro_referencia: OperacionNroReferencia,
      tipo: OperacionTipo,
      tipo_importacion: OperacionTipoImportacion,
      estado: OperacionEstado
    });
  }

  return {
    subirArchivo,
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
