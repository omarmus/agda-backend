'use strict';

const debug = require('debug')('app:service:cuenta');
const path = require('path');
const fs = require('fs');
const Service = require('../Service');

module.exports = function cuentaService (repositories, valueObjects, res) {
  const { CuentaRepository, ArchivoRepository } = repositories;

  async function findAll (params = {}, rol) {
    debug('Lista de cuenta|filtros');

    let lista;

    try {
      lista = await CuentaRepository.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.warning(new Error(`Error al obtener la lista de Cuenta`));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando cuenta por ID', id);

    return Service.findById(id, CuentaRepository, res, 'Cuenta');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar cuenta');

    return Service.createOrUpdate(data, CuentaRepository, res, 'Cuenta');
  }

  async function deleteItem (id) {
    debug('Eliminando cuenta');

    return Service.deleteItem(id, CuentaRepository, res, 'Cuenta');
  }

  async function subirArchivo (file, idUsuario) {
    try {
      const filename = `estado-de-cuenta-${Date.now()}.${file.name.split('.').pop()}`;
      const filepath = `public/uploads/${filename}`;
      const filesize = (file.size / 1024).toFixed(2);

      // Subiendo archivo
      await file.mv(path.join(process.cwd(), filepath));
      if (!fs.existsSync(path.join(process.cwd(), filepath))) {
        throw new Error('Se produjo un error al subir el archivo.');
      }

      // crear archivo en la base de datos
      let archivo = await ArchivoRepository.createOrUpdate({
        filename: filename,
        path: filepath,
        es_imagen: false,
        size: filesize,
        _user_created: idUsuario,
        _created_at: new Date()
      });

      if (!archivo) {
        return res.error(new Error('¡Ocurrió un error al subir la información de archivo!'));
      }

      return res.success({ archivo });
    } catch (e) {
      return res.error(e);
    }
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    subirArchivo
  };
};
