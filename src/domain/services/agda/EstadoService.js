'use strict';

const debug = require('debug')('app:service:estado');
const moment = require('moment');
const Service = require('../Service');
const { mail, text } = require('../../../common');
const template = require('../../../infrastructure/seeders/templates/correo-tmpl');

module.exports = function estadoService (repositories, valueObjects, res) {
  const { EstadoRepository, OperacionRepository } = repositories;
  // const {
  //   EstadoNombre,
  //   EstadoFechaSolicitud,
  //   EstadoFechaActualizacion,
  //   EstadoObservacion,
  //   EstadoEstado
  // } = valueObjects;

  async function findAll (params, rol) {
    debug('Lista de estado|filtros');

    let lista;

    try {
      lista = await EstadoRepository.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.warning(new Error('Error al obtener la lista de Estado'));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando estado por ID', id);

    return Service.findById(id, EstadoRepository, res, 'Estado');
  }

  async function createOrUpdate (data, email = false) {
    debug('Crear o actualizar estado', data, email);

    if (email && data.estado) {
      const ope = await OperacionRepository.findById(data.id_operacion);
      if (data.nombre === 'VALIDACIÓN') {
        await OperacionRepository.createOrUpdate({ id: data.id_operacion, estado: 'PENDIENTE_PAGO' });
        const mensaje = `Distinguido cliente, le informamos que el despacho con número de factura ${ope.nro_factura}, se encuentra listo para ser pagado, si tiene alguna consulta puede ingresar al sistema o comunicarse con nuestras oficinas. El monto del pago es de ${ope.monto_pago} Bs correspondiente al pago de tributos.`;
        await sendEmail(mensaje, 'Pendiente de pago', data.email);
      }
      if (data.nombre === 'PAGO DE TRIBUTOS') {
        await OperacionRepository.createOrUpdate({ id: data.id_operacion, fecha_pago: new Date() });

        if (['ANTICIPADA', 'INMEDIATA'].includes(ope.tipo_importacion)) {
          await OperacionRepository.createOrUpdate({ id: data.id_operacion, estado: 'PENDIENTE_REGULARIZACION' });
          const mensaje = `Distinguido cliente, le informamos que el despacho con número de factura ${ope.nro_factura} posee documentos que requieren ser regularizados, ingrese al sistema o comuníquese con nuestras oficinas para mayor información.`;
          await sendEmail(mensaje, 'Pendiente de regularización de documentos', data.email);
        }
      }

      if (data.nombre === 'DESPACHO CONCLUÍDO') {
        await OperacionRepository.createOrUpdate({ id: data.id_operacion, estado: 'CONCLUIDO' });
        const mensaje = `Distinguido cliente, le informamos que se concluyó el despacho con número de factura ${ope.nro_factura}, gracias por confiar en nosotros.`;
        await sendEmail(mensaje, 'Concluido', data.email);
      }
    }

    return Service.createOrUpdate(data, EstadoRepository, res, 'Estado');
  }

  async function cambiarSujetoMulta () {
    try {
      const lista = await EstadoRepository.findOperations();

      const items = [];
      const alertas = [];
      for (const item of lista.rows) {
        const a = moment();
        const b = moment(item.fecha_final);
        const days = a.diff(b, 'days');

        if (days >= 20) {
          items.push(item);
        } else if (days === 17) {
          alertas.push(item);
        }

        console.log('DIAS:', days, 'OPE:', item.id_operacion);
      }
      console.log('ALERTAS', alertas);
      console.log('ITEMS', items);
      for (const item of alertas) {
        const mensaje = `Solo quedan 3 días para realizar la regularización de documentos del despacho con número de factura ${item.operacion.nro_factura}. Por favor realice las gestiones pertinentes.`;
        await sendEmail(mensaje, 'Alerta para no ser sujeto a multa por plazo vencido', item.operacion.cliente.email);
      }
      for (const item of items) {
        await OperacionRepository.createOrUpdate({ id: item.id_operacion, estado: 'SUJETO_MULTA' });
        const mensaje = `Distinguido cliente, le informamos que el despacho con número de factura ${item.operacion.nro_factura} posee una multa pendiente por vencimiento de plazo, ingrese al sistema o comuníquese con nuestras oficinas para mayor información.`;
        await sendEmail(mensaje, 'Sujeto a multa por plazo vencido', item.operacion.cliente.email);
      }
      return res.success(true);
    } catch (e) {
      return res.error(e);
    }
  }

  async function sendEmail (mensaje, title, email) {
    const html = text.nano(template, {
      title,
      mensaje,
      year: new Date().getFullYear()
    });
    try {
      const correo = await mail.enviar({
        para: email,
        titulo: title,
        html
      });
      console.log('SUCCESS MAIL', correo);
    } catch (e) {
      console.log('ERROR MAIL', e.message, e);
    }
  }

  async function deleteItem (id) {
    debug('Eliminando estado');

    return Service.deleteItem(id, EstadoRepository, res, 'Estado');
  }

  // function validate (data) {
  //   Service.validate(data, {
  //     nombre: EstadoNombre,
  //     fecha_solicitud: EstadoFechaSolicitud,
  //     fecha_actualizacion: EstadoFechaActualizacion,
  //     observacion: EstadoObservacion,
  //     estado: EstadoEstado
  //   });
  // }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem,
    cambiarSujetoMulta
  };
};
