'use strict';

const debug = require('debug')('app:service:plantilla');
const Service = require('../Service');

module.exports = function plantillaService (repositories, valueObjects, res) {
  const { PlantillaRepository, PlantillaDocumentosRepository } = repositories;
  // const {
  //   PlantillaNombre,
  //   PlantillaFechaSolicitud,
  //   PlantillaFechaActualizacion,
  //   PlantillaObservacion,
  //   PlantillaEstado
  // } = valueObjects;

  async function findAll (params = {}, rol = null) {
    debug('Lista de plantilla|filtros');

    let lista;

    try {
      lista = await PlantillaRepository.findAll(params);

      for (const item of lista.rows) {
        const plantilla = await PlantillaDocumentosRepository.findAll({ id_plantilla: item.id });
        item.documentos = plantilla.rows;
      }
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.warning(new Error('Error al obtener la lista de Plantilla'));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando plantilla por ID', id);

    const plantilla = await PlantillaRepository.findById(id);

    const documentos = await PlantillaDocumentosRepository.findAll({ id_plantilla: id });

    plantilla.documentos = documentos.rows;

    return res.success(plantilla);
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar plantilla');

    // validate(data);
    let plantilla;
    try {
      plantilla = await PlantillaRepository.createOrUpdate(data);

      if (data.documentos) {
        // if (data.id) {
        //   const plantillas = await PlantillaDocumentosRepository.findAll({ id_plantilla: plantilla.id });
        //   for (const item of plantillas.rows) {
        //     await PlantillaDocumentosRepository.deleteItem(item.id);
        //   }
        // }
        for (const item of data.documentos) {
          if (!item.documento) {
            await PlantillaDocumentosRepository.createOrUpdate({
              id_plantilla: plantilla.id,
              id_documento: item.id,
              _user_created: data._user_created || data._user_updated,
              _created_at: new Date()
            });
          }
        }
      }
    } catch (e) {
      return res.error(e);
    }

    if (!plantilla) {
      return res.warning(new Error('La plantilla no pudo ser creada'));
    }

    return res.success(plantilla);
  }

  async function deleteItem (id) {
    debug('Eliminando plantilla');

    return Service.deleteItem(id, PlantillaRepository, res, 'Plantilla');
  }

  // function validate (data) {
  //   Service.validate(data, {
  //     nombre: PlantillaNombre,
  //     fecha_solicitud: PlantillaFechaSolicitud,
  //     fecha_actualizacion: PlantillaFechaActualizacion,
  //     observacion: PlantillaObservacion,
  //     estado: PlantillaEstado
  //   });
  // }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
