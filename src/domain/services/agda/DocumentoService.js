'use strict';

const debug = require('debug')('app:service:documento');
const Service = require('../Service');
const path = require('path');
const fs = require('fs');
const moment = require('moment');

module.exports = function documentoService (repositories, valueObjects, res) {
  const { DocumentoRepository, ArchivoRepository } = repositories;
  const {
    DocumentoNombre,
    DocumentoFechaSolicitud,
    DocumentoFechaActualizacion,
    DocumentoObservacion,
    DocumentoEstado
  } = valueObjects;

  async function findAll (params, rol) {
    debug('Lista de documento|filtros');

    let lista;

    try {
      if (params.id_cliente) {
        lista = await DocumentoRepository.findAll(params);
        const ids = [];
        lista.rows.forEach(item => {
          if (item.id_documento_padre) {
            ids.push(item.id_documento_padre);
          }
        });
        delete params.id_cliente;
        params.tipo = 'CLIENTE';
        const generales = await DocumentoRepository.findAll(params);
        const items = [];
        generales.rows.forEach(item => {
          if (ids.indexOf(item.id) === -1) {
            items.push(item);
          }
        });
        lista.rows = items.concat(lista.rows);
        lista.count = items.length;
      } else {
        lista = await DocumentoRepository.findAll(params);
      }
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.warning(new Error('Error al obtener la lista de Documento'));
    }

    return res.success(lista);
  }

  async function findById (id) {
    debug('Buscando documento por ID', id);

    return Service.findById(id, DocumentoRepository, res, 'Documento');
  }

  async function createOrUpdate (data) {
    debug('Crear o actualizar documento');

    validate(data);

    return Service.createOrUpdate(data, DocumentoRepository, res, 'Documento');
  }

  async function deleteItem (id) {
    debug('Eliminando documento');

    return Service.deleteItem(id, DocumentoRepository, res, 'Documento');
  }

  function validate (data) {
    Service.validate(data, {
      nombre: DocumentoNombre,
      fecha_solicitud: DocumentoFechaSolicitud,
      fecha_actualizacion: DocumentoFechaActualizacion,
      observacion: DocumentoObservacion,
      estado: DocumentoEstado
    });
  }

  async function subirDocumento (file, idDocumento, idUsuario, idCliente, tipo, idOperacion, update = false) {
    debug('Subiendo archivo');

    let documento;
    try {
      const filename = `DOC-${idDocumento}-${Date.now()}.${file.name.split('.').pop()}`;
      const filepath = `public/uploads/${filename}`;
      const filesize = (file.size / 1024).toFixed(2);

      // Subiendo archivo
      await file.mv(path.join(process.cwd(), filepath));
      if (!fs.existsSync(path.join(process.cwd(), filepath))) {
        throw new Error('Se produjo un error al subir el archivo.');
      }

      // crear archivo en la base de datos
      const archivo = await ArchivoRepository.createOrUpdate({
        filename: filename,
        path: filepath,
        es_imagen: false,
        size: filesize,
        _user_created: idUsuario,
        _created_at: new Date()
      });

      if (!archivo) {
        return res.error(new Error('Ocurrió un error al subir la información de archivo!'));
      }

      // Creando documento del cliente
      documento = await DocumentoRepository.findById(idDocumento);

      documento = {
        id: update ? idDocumento : null,
        nombre: documento.nombre,
        fecha_actualizacion: documento.fecha_actualizacion,
        fecha_solicitud: moment().format('YYYY-MM-DD'),
        observacion: documento.observacion,
        tipo,
        estado: 'PENDIENTE',
        id_archivo: archivo.id,
        id_cliente: idCliente,
        id_operacion: idOperacion,
        id_documento_padre: idDocumento,
        _user_created: idUsuario,
        _created_at: new Date()
      };

      documento = await DocumentoRepository.createOrUpdate(documento);

      return res.success(documento);
    } catch (e) {
      return res.error(e);
    }
  }

  return {
    subirDocumento,
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
