'use strict';

const Text = require('../../general/Text');

class DocumentoObservacion extends Text {
  constructor (value, errors) {
    super('observacion', value, {}, errors);
  }
}

module.exports = DocumentoObservacion;
