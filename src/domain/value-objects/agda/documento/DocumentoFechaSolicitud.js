
'use strict';

const Datetime = require('../../general/Datetime');

class DocumentoFechaSolicitud extends Datetime {
  constructor (value, errors) {
    super('fecha_solicitud', value, {}, errors);
  }
}

module.exports = DocumentoFechaSolicitud;
