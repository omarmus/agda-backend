
'use strict';

const Datetime = require('../../general/Datetime');

class DocumentoFechaActualizacion extends Datetime {
  constructor (value, errors) {
    super('fecha_actualizacion', value, {}, errors);
  }
}

module.exports = DocumentoFechaActualizacion;
