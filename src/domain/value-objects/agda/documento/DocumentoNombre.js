'use strict';

const Text = require('../../general/Text');

class DocumentoNombre extends Text {
  constructor (value, errors) {
    super('nombre', value, { maxlength: 150, required: true }, errors);
  }
}

module.exports = DocumentoNombre;
