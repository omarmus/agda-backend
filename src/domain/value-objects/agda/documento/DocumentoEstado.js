'use strict';

const Enum = require('../../general/Enum');

class DocumentoEstado extends Enum {
  constructor (value, errors) {
    const values = ['PENDIENTE', 'VERIFICADO', 'OBSERVADO', 'ACTIVO', 'INACTIVO'];
    super('estado', value, { required: true, values }, errors);
  }
}

module.exports = DocumentoEstado;
