'use strict';

const Enum = require('../../general/Enum');

class DocumentoEstado extends Enum {
  constructor (value, errors) {
    const values = ['CONSUMO_DIRECTO', 'ANTICIPADA', 'INMEDIATA', 'ABREVIADA'];
    super('tipo_importacion', value, { required: true, values }, errors);
  }
}

module.exports = DocumentoEstado;
