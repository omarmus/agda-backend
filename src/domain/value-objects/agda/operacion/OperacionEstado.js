'use strict';

const Enum = require('../../general/Enum');

class DocumentoEstado extends Enum {
  constructor (value, errors) {
    const values = ['PROCESO', 'PENDIENTE_PAGO', 'PENDIENTE_REGULARIZACION', 'CONCLUIDO'];
    super('estado', value, { required: true, values }, errors);
  }
}

module.exports = DocumentoEstado;
