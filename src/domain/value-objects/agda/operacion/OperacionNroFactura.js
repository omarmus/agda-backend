'use strict';

const Text = require('../../general/Text');

class DocumentoNombre extends Text {
  constructor (value, errors) {
    super('nro_factura', value, { maxlength: 100, required: false }, errors);
  }
}

module.exports = DocumentoNombre;
