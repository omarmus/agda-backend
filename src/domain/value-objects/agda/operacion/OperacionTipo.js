'use strict';

const Enum = require('../../general/Enum');

class DocumentoEstado extends Enum {
  constructor (value, errors) {
    const values = ['IMPORTACION', 'EXPORTACION'];
    super('tipo', value, { required: true, values }, errors);
  }
}

module.exports = DocumentoEstado;
