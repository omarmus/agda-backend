# Base Backend

Proyecto base para desarrollo de backends usando node.js, es un mini-framework que utiliza la arquitecutra [ddd](doc/Arquitectura.md).

Este base contiene los siguientes módulos:

- Entidades
- Usuarios
- Personas
- Roles
- Módulos
- Permisos
- Tokens
- Ciudadanía Digital

También ya se integra los siguientes módulos con sus respectivos seeders

- Logs del sistema
- Parámetros del sistema
- Servicios de Interoperabilidad 

Funcionalidades incluidas

- Integración con ciudadanía Digital (*probar*)
- Módulo para deploy del sistema a producción
- Ejemplo de uso para cronjobs
- Buscar reemplazo o ver si se puede utilizar app-Iop

# Sobre el proyecto

* Licencia y lista de colaboradores
  - Licencia: [LPG-Bolivia v1.0](LICENCIA.md)
  - Lista de personas autoras: [COLABORACIÓN.md](COLABORACIÓN.md)
* Instalación
  - Especificación a detalle en archivo [INSTALL.md](INSTALL.md)
* Documentación estrucutra del proyecto y código fuente `doc/`
  - Arquitectura del proyecto: [Arquitectura.md](doc/Arquitectura.md)
  - Estructura del código fuente: [Codigo.md](doc/Codigo.md)
* Por Hacer
  - Detalles: [TODO.md](TODO.md)














